"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mqtt = require("mqtt");
var MqttClient = /** @class */ (function () {
    function MqttClient(brokerUrl, options) {
        this.client = mqtt.connect(brokerUrl, options);
        this.client.on('connect', function (err) {
            console.log('Connected to MQTT Broker');
        });
        this.client.on('error', function (error) {
            console.error('MQTT client error:', error);
        });
        this.client.on('message', function (topic, message) {
            console.log("Received message on ".concat(topic));
            console.log("Received message on ".concat(topic, ": ").concat(messageToString(message)));
        });
    }
    MqttClient.prototype.subscribe = function (topic) {
        this.client.subscribe(topic, function (err) {
            if (!err) {
                console.log("Subscribed to topic: ".concat(topic));
            }
            else {
                console.error("Failed to subscribe to ".concat(topic, ": ").concat(err));
            }
        });
    };
    MqttClient.prototype.publish = function (topic, message) {
        this.client.publish(topic, message, function (err) {
            if (!err) {
                console.log("Published to ".concat(topic, ": ").concat(message));
            }
            else {
                console.error("Failed to publish to ".concat(topic, ": ").concat(err));
            }
        });
    };
    return MqttClient;
}());
// SSL options
var options = {
    port: 8884,
    protocolVersion: 5,
    protocol: 'tcp',
    // protocolVersion: 3,
    // protocol: 'ssl',
    // key: fs.readFileSync('path-to-key.pem'),  // Replace with the path to your key
    // cert: fs.readFileSync('path-to-cert.pem'),  // Replace with the path to your certificate
    // ca: [fs.readFileSync('path-to-ca.crt')]  // Replace with the path to your CA certificate
    // password: '123456',
};
var transactionId = '111111111';
// const secureClient = new MqttClient('mqtts://your-broker-url', options);
var client = new MqttClient('tcp://192.168.10.60:8884', options);
client.subscribe("Transaction/".concat(transactionId));
client.subscribe('Failures'); // description: When a failure occurs, an ExceptionEnvelope is published to this Topic.
var FUAdded = "500000000E000000000482020000"; //FU Added: 
var CardInserted = "500000000E000000000484020000"; //Card inserted: 
var CardRemoved = "500000000E000000000485020000"; //Card removed: 
var FuRemoved = "500000000E000000000483020000"; //Fu removed: 
var fuAddedPayload = {
    transactionId: transactionId, messages: [FUAdded]
};
var cardInPayload = {
    transactionId: transactionId, messages: [CardInserted]
};
var CardRemovedPayload = {
    transactionId: transactionId, messages: [CardRemoved]
};
var FuRemovedPayload = {
    transactionId: transactionId, messages: [FuRemoved]
};
setTimeout(function () {
    // client.publish('CardTerminal', objectToBuffer(CardRemovedPayload));
    // client.publish('CardTerminal', objectToBuffer(FuRemovedPayload));
}, 3000);
setTimeout(function () {
    client.publish('CardTerminal', objectToBuffer(fuAddedPayload));
    client.publish('CardTerminal', objectToBuffer(cardInPayload));
}, 2000);
function objectToBuffer(obj) {
    var jsonString = JSON.stringify(obj);
    var buffer = Buffer.from(jsonString, 'utf-8');
    return buffer;
}
function messageToString(message) {
    return message.toString('hex');
}
