import * as mqtt from 'mqtt';
import * as fs from 'fs';

class MqttClient {
    private client: mqtt.MqttClient;

    constructor(brokerUrl: string, options?: mqtt.IClientOptions) {
        this.client = mqtt.connect(brokerUrl, options);

        this.client.on('connect', (err) => {
            console.log('Connected to MQTT Broker');
        });

		this.client.on('error', (error) => {
			console.error('MQTT client error:', error)
		})

        this.client.on('message', (topic, message) => {
            console.log(`Received message on ${topic}`)
            console.log(`Received message on ${topic}: ${messageToString(message)}`);
        });
    }

    subscribe(topic: string): void {
        this.client.subscribe(topic, (err) => {
            if (!err) {
                console.log(`Subscribed to topic: ${topic}`);
            } else {
                console.error(`Failed to subscribe to ${topic}: ${err}`);
            }
        });
    }

    publish(topic: string, message: string | Buffer): void {
        this.client.publish(topic, message, (err) => {
            if (!err) {
                console.log(`Published to ${topic}: ${message}`);
            } else {
                console.error(`Failed to publish to ${topic}: ${err}`);
            }
        });
    }
}

// SSL options
const options: mqtt.IClientOptions = {
    port: 8884,
	protocolVersion: 5,
    protocol: 'tcp',
    // protocolVersion: 3,
    // protocol: 'ssl',
    // key: fs.readFileSync('path-to-key.pem'),  // Replace with the path to your key
    // cert: fs.readFileSync('path-to-cert.pem'),  // Replace with the path to your certificate
    // ca: [fs.readFileSync('path-to-ca.crt')]  // Replace with the path to your CA certificate
    // password: '123456',
};

const transactionId = '111111111';

// const secureClient = new MqttClient('mqtts://your-broker-url', options);
const client = new MqttClient('tcp://192.168.10.60:8884', options);

client.subscribe(`Transaction/${transactionId}`);
client.subscribe('Failures'); // description: When a failure occurs, an ExceptionEnvelope is published to this Topic.

const FUAdded = "500000000E000000000482020000" //FU Added: 
const CardInserted = "500000000E000000000484020000" //Card inserted: 

const CardRemoved = "500000000E000000000485020000" //Card removed: 
const FuRemoved = "500000000E000000000483020000" //Fu removed: 
const fuAddedPayload = {
    transactionId: transactionId, messages: [FUAdded]
}

const cardInPayload = {
    transactionId: transactionId, messages: [CardInserted]
}

const CardRemovedPayload = {
    transactionId: transactionId, messages: [CardRemoved]
}

const FuRemovedPayload = {
    transactionId: transactionId, messages: [FuRemoved]
}

setTimeout(() => {

    // client.publish('CardTerminal', objectToBuffer(CardRemovedPayload));
    // client.publish('CardTerminal', objectToBuffer(FuRemovedPayload));

}, 3000)

setTimeout(() => {
client.publish('CardTerminal', objectToBuffer(fuAddedPayload));
client.publish('CardTerminal', objectToBuffer(cardInPayload));

}, 2000)

function objectToBuffer(obj: any): Buffer {
    const jsonString = JSON.stringify(obj);
    const buffer = Buffer.from(jsonString, 'utf-8');
    return buffer;
}

function messageToString(message: Buffer): string {
    return message.toString('hex');
}

